﻿### 概要
工数の可視化ツール

### 利用しているライブラリ（ライセンス）
[Python](https://www.python.org/) (Python Software Foundation License)
[django](http://djangoproject.jp/) (BSD License)
[django-bootstrap4](https://pypi.org/project/django-bootstrap4/) (BSD License)
[django-bootstrap-datepicker-plus](https://pypi.org/project/django-bootstrap-datepicker-plus/) (Apache Software License)
[jpholiday](https://pypi.org/project/jpholiday/) (MIT License)
[markdown](https://pypi.org/project/Markdown/) (BSD License)
[Bootstrap](https://getbootstrap.com/) (MIT License)
[Chart.js](https://github.com/chartjs/Chart.js) (MIT License)
