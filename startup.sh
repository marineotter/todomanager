#!/bin/bash
set -eu
# touch data/db.sqlite3
# chown app data/db.sqlite3
python manage.py migrate
python manage.py custom_createsuperuser --username admin --email admin --password admin
nginx -p /app -c nginx.conf
gunicorn --bind=127.0.0.1:8000 --workers=2 todomanager.wsgi
