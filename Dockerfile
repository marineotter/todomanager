FROM python:3.9-slim-buster
WORKDIR /app

RUN apt-get update && apt-get install -y \
  nginx \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt && \
    rm -rf ~/.cache/pip

COPY . /app
RUN python manage.py collectstatic --noinput
RUN useradd --create-home -u 1000 app && \
    mkdir /app/data && chown app /app/data && \
    chown -R app /var/lib/nginx && chown -R app /var/log/nginx && chown -R app /run
USER app
CMD ["/bin/bash", "./startup.sh"]
EXPOSE 80
