from django.db import models
from django import forms
from datetime import date, timedelta
import bootstrap_datepicker_plus as datetimepicker
import markdown
import jpholiday


def is_business_day(value: date):
    if value.weekday() >= 5:  # 土日
        return False
    if jpholiday.is_holiday(value):
        return False
    return True


def get_business_days(start: date, end: date):
    if end < start:
        raise ValueError("get_business_days: end < start")
    else:
        result = []
        for i in range((end - start).days + 1):
            value = start + timedelta(days=i)
            if (is_business_day(value)):
                result.append(start + timedelta(days=i))
        return result


# start_dateからend_dateまでの営業日数を返す
def get_business_days_count(day1: date, day2: date):
    if day1 <= day2:
        return len(get_business_days(day1, day2)) - 1
    else:
        return -len(get_business_days(day2, day1)) + 1


class TaskLabel(models.Model):
    name = models.CharField(max_length=128, verbose_name="ラベル名")
    color = models.CharField(max_length=32, verbose_name="色")

    def __str__(self):
        return self.name


class Task(models.Model):
    title = models.CharField(max_length=128, verbose_name="タイトル")
    start_date = models.DateField(verbose_name="開始日", default=date.today)
    target_date = models.DateField(verbose_name="期日", default=date.today)
    assumed_workload = models.FloatField(verbose_name="予定工数（時間）")
    note = models.TextField(verbose_name="ノート", blank=True)
    labels = models.ManyToManyField(TaskLabel, verbose_name="ラベル",
                                    related_name="tasks", blank=True)
    color = models.CharField(
        max_length=32, verbose_name="色", default="#ffffff")

    def __str__(self):
        return self.title

    def get_note_html(self):
        md = markdown.Markdown()
        return md.convert(self.note)

    # 残日数。
    def remaining_days(self, end_date=None, allow_zero=True):
        if end_date is None:
            value = get_business_days_count(date.today(), self.target_date)
        else:
            value = get_business_days_count(end_date, self.target_date)
        return value if value < 0 or allow_zero else value + 1

    # end_date時点までの総進捗。
    def total_actual_progress(self, end_date=None):
        use_date = end_date if end_date is not None else date.today()
        ret = self.actual_data.filter(input_date__lte=use_date).aggregate(
            models.Sum("progress"))
        value = ret["progress__sum"]
        if value is None:
            return 0
        elif value > 100:
            return 100
        elif value < 0:
            return 0
        else:
            return round(value, 0)

    # end_date時点までの総実績工数。
    def total_actual_workload(self, end_date=None):
        use_date = end_date if end_date is not None else date.today()
        total_actual_progress = self.total_actual_progress(use_date)
        ret = self.actual_data.filter(input_date__lte=use_date).aggregate(
            models.Sum("actual_workload"))
        value = ret["actual_workload__sum"]
        if value is None:
            return 0
        elif total_actual_progress < 0.001:
            # 合計進捗が0%以下のとき、例外的に実績工数は0とする。
            return 0
        else:
            return value

    # end_date時点での予測総工数。
    def predicted_total_workload(self, end_date=None):
        use_date = end_date if end_date is not None else date.today()
        total_actual_workload = self.total_actual_workload(use_date)
        total_actual_progress = self.total_actual_progress(use_date)
        if end_date is not None and end_date < self.start_date:
            return 0
        elif total_actual_progress < 0.001:
            # 合計進捗が0%以下のとき、例外的に予測残工数は予定工数とする。
            return self.assumed_workload
        else:
            return round(total_actual_workload * 100 / total_actual_progress, 1)

    # end_date時点での予測残工数。
    def predicted_remaining_workload(self, end_date=None):
        use_date = end_date if end_date is not None else date.today()

        if use_date <= date.today():
            predicted_total_workload = self.predicted_total_workload(use_date)
            total_actual_workload = self.total_actual_workload(use_date)
            return round(predicted_total_workload - total_actual_workload, 1)
        else:
            predicted_total_workload = self.predicted_total_workload(
                date.today())
            total_actual_workload = self.total_actual_workload(date.today())
            last_remaining_workload = predicted_total_workload - total_actual_workload
            remain_total_days = self.remaining_days(date.today()) + 1
            remain_current_days = self.remaining_days(use_date) + 1
            if remain_total_days > 0:
                value = round(last_remaining_workload *
                              remain_current_days / remain_total_days, 1)
                return value if value > 0 else 0
            else:
                return 0

    # end_date時点での残一日あたりの工数。
    def predicted_workload_per_day(self, end_date=None):
        use_date = end_date if end_date is not None else date.today()
        remain_current_days = self.remaining_days(use_date) + 1
        if remain_current_days > 0:
            value = self.predicted_remaining_workload(
                use_date) / remain_current_days
            return round(value, 1)
        else:
            return 0

    # end_dateの実績工数（未来日時の場合は予測１日あたり工数。）
    def actual_workload(self, end_date=None):
        use_date = end_date if end_date is not None else date.today()
        if use_date <= date.today():
            ret = self.actual_data.filter(input_date=use_date).aggregate(
                models.Sum("actual_workload"))
            value = ret["actual_workload__sum"]
            if value is None or value < 0:
                return 0
            else:
                return round(value, 1)
        else:
            return self.predicted_workload_per_day(end_date)

    # end_dateの進捗
    def actual_progress(self, end_date=None):
        use_date = end_date if end_date is not None else date.today()
        if use_date <= date.today():
            ret = self.actual_data.filter(input_date=use_date).aggregate(
                models.Sum("progress"))
            value = ret["progress__sum"]
            if value is None or value < 0:
                return 0
            else:
                return round(value, 0)
        else:
            return self.predicted_workload_per_day(end_date)


class ActualData(models.Model):
    input_date = models.DateField(verbose_name="投入日", default=date.today)
    actual_workload = models.FloatField(verbose_name="実績工数")
    progress = models.FloatField(verbose_name="進捗増分")
    note = models.TextField(verbose_name="ノート", blank=True)

    parent_task = models.ForeignKey(Task, verbose_name="親タスク",
                                    related_name="actual_data",
                                    on_delete=models.CASCADE)

    class Meta:
        ordering = ['-input_date', '-pk']

    def __str__(self):
        return "{} ({}h {}%)".format(self.input_date,
                                     self.actual_workload,
                                     self.progress)

    def get_note_html(self):
        md = markdown.Markdown()
        return md.convert(self.note)


class TaskAddForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('title', 'start_date', 'target_date',
                  'assumed_workload', 'note', 'labels', 'color')
        widgets = {
            'title': forms.TextInput(attrs={'size': 128}),
            'color': forms.TextInput(attrs={'type': 'color'}),
            'start_date': datetimepicker.DatePickerInput(
                format='%Y-%m-%d',
                options={
                    'locale': 'ja',
                    'dayViewHeaderFormat': 'YYYY年 MMMM',
                }),
            'target_date': datetimepicker.DatePickerInput(
                format='%Y-%m-%d',
                options={
                    'locale': 'ja',
                    'dayViewHeaderFormat': 'YYYY年 MMMM',
                }),
            'assumed_workload': forms.NumberInput(attrs={'step': '0.5'}),
            'note': forms.Textarea(attrs={'cols': 80, 'rows': 6}),
            'labels': forms.CheckboxSelectMultiple
        }


class TaskLabelAddForm(forms.ModelForm):
    class Meta:
        model = TaskLabel
        fields = ('name', 'color')
        widgets = {
            'name': forms.TextInput(attrs={'size': 128}),
            'color': forms.TextInput(attrs={'type': 'color'})
        }


class ActualDataAddForm(forms.ModelForm):
    class Meta:
        model = ActualData
        fields = ('input_date', 'actual_workload', 'progress', 'note')
        widgets = {
            'input_date': datetimepicker.DatePickerInput(
                format='%Y-%m-%d',
                options={
                    'locale': 'ja',
                    'dayViewHeaderFormat': 'YYYY年 MMMM',
                }),
            'actual_workload': forms.NumberInput(attrs={'step': "0.1"}),
            'progress': forms.NumberInput(attrs={'step': "1"}),
            'note': forms.Textarea(attrs={'cols': 80, 'rows': 6}),
        }
