# Generated by Django 2.1.7 on 2019-03-30 03:07

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20190330_1122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actualdata',
            name='date',
            field=models.DateField(default=datetime.date.today, verbose_name='投入日'),
        ),
        migrations.AlterField(
            model_name='task',
            name='note',
            field=models.TextField(blank=True, verbose_name='ノート'),
        ),
        migrations.AlterField(
            model_name='task',
            name='start_date',
            field=models.DateField(default=datetime.date.today, verbose_name='開始日'),
        ),
        migrations.AlterField(
            model_name='task',
            name='target_date',
            field=models.DateField(default=datetime.date.today, verbose_name='期日'),
        ),
    ]
