from django.views.generic import TemplateView, CreateView, UpdateView
from django.views import View
from .models import Task, TaskLabel, ActualData
from .models import TaskAddForm, TaskLabelAddForm, ActualDataAddForm
from django.urls import reverse
from datetime import date, timedelta
from django.http import JsonResponse
import jpholiday
import parse
# Create your views here.


class TopPageView(TemplateView):
    template_name = "top_page.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['remain_task_list'] = []
        context['completed_task_list'] = []
        for task in Task.objects.order_by("target_date", "pk"):
            if task.total_actual_progress() < 100:
                context['remain_task_list'].append(task)
            else:
                context['completed_task_list'].append(task)
        context['all_task_list'] = context['remain_task_list'] + context['completed_task_list']
        return context


class TopGraphDataView(View):
    def color_convert(self, code, alpha):
        base_format = "rgba({} , {} , {} , {})"
        cleansed_code = code.strip()
        if len(cleansed_code) != 7:
            return ""
        else:
            try:
                val_r = parse.parse("{:x}", cleansed_code[1:3]).fixed[0]
                val_g = parse.parse("{:x}", cleansed_code[3:5]).fixed[0]
                val_b = parse.parse("{:x}", cleansed_code[5:7]).fixed[0]
            except Exception:
                return ""
        return base_format.format(val_r, val_g, val_b, alpha)

    def get(self, request, *args, **kwargs):
        param = request.GET.get(key="param", default="total")
        if param == "total":
            title = "予測工数"
        elif param == "perday":
            title = "予測工数/日"
        else:
            title = "実績工数/日"

        end_date = date.today()

        agg_day_list = [end_date]
        counter = 0
        for i in range(1, 100):
            tmp = end_date + timedelta(days=i)
            if tmp.weekday() < 5 and not jpholiday.is_holiday(tmp):
                agg_day_list.append(tmp)
                counter += 1
            if counter >= 30:
                break
        counter = 0
        for i in range(1, 100):
            tmp = end_date - timedelta(days=i)
            if tmp.weekday() < 5 and not jpholiday.is_holiday(tmp):
                agg_day_list.insert(0, tmp)
                counter += 1
            if counter >= 30:
                break

        # 初期化
        result_date = {
            "type": "line",
            "data": {
                "labels": [x.__str__() for x in agg_day_list],
                "datasets": [],
            },
            "options": {
                "title": {
                    "display": True,
                    "text": title
                },
                "tooltips": {
                    "callbacks": {}
                },
                "scales": {
                    "yAxes": [
                        {
                            "ticks": {
                                "beginAtZero": True
                            },
                            "stacked": True
                        }
                    ]
                },
                "plugins": {
                    "filler": {
                        "propagate": True
                    }
                }
            },
            "lineAtIndex": [30]
        }

        for task in Task.objects.order_by('target_date'):
            if task.start_date >= agg_day_list[0] or task.predicted_remaining_workload(agg_day_list[0]) > 0:
                if param == "total":
                    graph_data = [task.predicted_remaining_workload(x) for x in agg_day_list]
                elif param == "perday":
                    graph_data = [task.predicted_workload_per_day(x) for x in agg_day_list]
                else:
                    graph_data = [task.actual_workload(x) for x in agg_day_list]

                result_date["data"]["datasets"].append({
                    "label": task.title,
                    "data": graph_data,
                    "backgroundColor": self.color_convert(task.color, 0.2),
                    "pointBackgroundColor": [
                        (self.color_convert(
                            task.color, 1 if x <= end_date else 0.0)
                         )for x in agg_day_list
                    ],
                    "pointBorderColor": [
                        (self.color_convert(
                            task.color, 1 if x <= end_date else 0.0)
                         )for x in agg_day_list
                    ],
                    "borderColor": self.color_convert(task.color, 1),
                    "borderWidth": 1,
                    "lineTension": 0,
                    "stack": 1,
                    "fill": "origin" if len(result_date["data"]["datasets"]) == 0 else "-1"
                })
        return JsonResponse(result_date)


class TaskAddView(CreateView):
    model = Task
    template_name = "task_add.html"
    form_class = TaskAddForm
    success_url = "/"

class ActualDataAddView(CreateView):
    model = ActualData
    template_name = "actual_data_add.html"
    form_class = ActualDataAddForm
    success_url = "/"

    def form_valid(self, form):
        form.instance.parent_task = Task.objects.get(
            pk=self.kwargs["parent_task_pk"])
        form.save()
        self.success_url = reverse("main:task_detail",
                                   kwargs={'pk': self.kwargs["parent_task_pk"]}
                                   )
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['task'] = Task.objects.get(
            pk=self.kwargs["parent_task_pk"])
        return context


class ActualDataEditView(UpdateView):
    model = ActualData
    template_name = "actual_data_edit.html"
    form_class = ActualDataAddForm
    success_url = "/"

    def form_valid(self, form):
        self.success_url = reverse("main:task_detail",
                                   kwargs={'pk': form.instance.parent_task.pk}
                                   )
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['task'] = context['object'].parent_task
        return context

class TaskDetailView(UpdateView):
    model = Task
    template_name = "task_detail.html"
    form_class = TaskAddForm
    success_url = "/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sorted_actual_data"] = self.object.actual_data.order_by(
            "-input_date", "-pk")
        self.success_url = reverse("main:task_detail",
                                   kwargs={'pk': self.object.pk}
                                   )
        return context


class TaskLabelAddView(CreateView):
    model = TaskLabel
    template_name = "task_label_add.html"
    form_class = TaskLabelAddForm
    success_url = "/"
