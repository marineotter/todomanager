from django.contrib import admin
from django.urls import path, include
from .views import TopPageView, TopGraphDataView, TaskAddView, TaskDetailView, TaskLabelAddView
from .views import ActualDataAddView, ActualDataEditView

app_name = "main"

urlpatterns = [
    path('', TopPageView.as_view(), name="top_page"),
    path('task/add/', TaskAddView.as_view(), name="task_add"),
    path(r'task/detail/<int:pk>/',
         TaskDetailView.as_view(), name="task_detail"),
    path(r'actualdata/add/<int:parent_task_pk>/',
         ActualDataAddView.as_view(), name="actual_data_add"),
    path(r'actualdata/edit/<int:pk>/',
         ActualDataEditView.as_view(), name="actual_data_edit"),
    path('label/add/', TaskLabelAddView.as_view(), name="label_add"),
    path('api/topgraphdata/', TopGraphDataView.as_view(), name="top_graph_data"),
]
